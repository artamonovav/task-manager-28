package ru.t1.artamonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.model.Task;
import ru.t1.artamonov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-show-by-index";

    @NotNull
    private static final String DESCRIPTION = "Display task by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        @NotNull final Task task = getTaskService().findOneByIndex(userId, index);
        showTask(task);
    }

}
