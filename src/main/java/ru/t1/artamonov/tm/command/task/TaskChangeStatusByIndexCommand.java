package ru.t1.artamonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-change-status-by-index";

    @NotNull
    private static final String DESCRIPTION = "Change task status by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);
        @NotNull final String userId = getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, status);
    }

}
