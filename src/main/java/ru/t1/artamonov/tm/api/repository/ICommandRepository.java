package ru.t1.artamonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(@Nullable AbstractCommand command);

    AbstractCommand getCommandByArgument(@Nullable String argument);

    AbstractCommand getCommandByName(@Nullable String name);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

}
